/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GraphBase.h
 * Author: erfan
 *
 * Created on October 15, 2019, 11:31 AM
 */

#ifndef GRAPHBASE_H
#define GRAPHBASE_H

#include <iostream>
#include <fstream>
#include <list>

#include <boost/graph/astar_search.hpp>

#include <boost/graph/graphviz.hpp>

#include "algorithms/AlgorithmBase.h"

#include <jsoncpp/json/json.h>

namespace UCal{
  using namespace boost;
  using namespace std;
  
  

  // specify some types
  typedef float cost;
  typedef adjacency_list<listS, vecS, undirectedS, no_property,
    property<edge_weight_t, cost> > graph_t;
  typedef property_map<graph_t, edge_weight_t>::type weightMap_t;
  typedef graph_t::vertex_descriptor                 vertex_descriptor_t;
  typedef graph_t::edge_descriptor                   edge_descriptor_t;
  typedef graph_t::vertex_iterator                   vertex_iterator_t;
  typedef std::pair<int, int>                        edge_t;

struct Location{
public:
    float x,y;
};//end struct location


template <class Name, class LocMap>
class city_writer {
public:
  city_writer(Name n, LocMap l, float _minx, float _maxx,
              float _miny, float _maxy,
              unsigned int _ptx, unsigned int _pty)
    : name(n), loc(l), minx(_minx), maxx(_maxx), miny(_miny),
      maxy(_maxy), ptx(_ptx), pty(_pty) {}
  template <class Vertex>
  void operator()(ostream& out, const Vertex& v) const {
    float px = 1 - (loc[v].x - minx) / (maxx - minx);
    float py = (loc[v].y - miny) / (maxy - miny);
    out << "[label=\"" << name[v] << "\", pos=\""
        << static_cast<unsigned int>(ptx * px) << ","
        << static_cast<unsigned int>(pty * py)
        << "\", fontsize=\"11\"]";
  }
private:
  Name name;
  LocMap loc;
  float minx, maxx, miny, maxy;
  unsigned int ptx, pty;
};//end class 


// euclidean distance heuristic
template <class Graph, class CostType, class LocMap>
class distance_heuristic : public astar_heuristic<Graph, CostType>
{
public:
  typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
  distance_heuristic(LocMap l, Vertex goal)
    : m_location(l), m_goal(goal) {}
  CostType operator()(Vertex u)
  {
    CostType dx = m_location[m_goal].x - m_location[u].x;
    CostType dy = m_location[m_goal].y - m_location[u].y;
    return ::sqrt(dx * dx + dy * dy);
  }
private:
  LocMap m_location;
  Vertex m_goal;
};


struct found_goal {}; // exception for termination

// visitor that terminates when we find the goal
template <class Vertex>
class astar_goal_visitor : public boost::default_astar_visitor
{
public:
  astar_goal_visitor(Vertex goal) : m_goal(goal) {}
  template <class Graph>
  void examine_vertex(Vertex u, Graph& g) {
    if(u == m_goal)
      throw found_goal();
  }
private:
  Vertex m_goal;
};

template <class WeightMap>
class time_writer {
public:
  time_writer(WeightMap w) : wm(w) {}
  template <class Edge>
  void operator()(ostream &out, const Edge& e) const {
    out << "[label=\"" << wm[e] << "\", fontsize=\"11\"]";
  }
private:
  WeightMap wm;
};

            
class GraphBase : public AlgorithmBase{
public:
    void init() override{
        ifstream ifs("json/loadGraph.json");
        Json::Reader  reader;
        Json::Value   obj;
        reader.parse(ifs, obj); // reader can also read strings
        const Json::Value& nodes = obj["nodes"];
        for(int i = 0; i < nodes.size(); i++){
            _nodes.push_back(nodes[i].asInt());
            
        }//end for 
        const Json::Value& locations = obj["locations"];
        for(int i = 0; i < locations.size(); i++){
            Location locationTmp;
            locationTmp.x = locations[i]["X"].asFloat();
            locationTmp.y = locations[i]["Y"].asFloat();
            _locations.push_back(locationTmp);
        }//end for 
        
        const Json::Value& edges = obj["edges"];
        for(int i = 0; i < edges.size(); i++){
            edge_t edgeTmp;
            edgeTmp.first  = edges[i]["START"].asInt();
            edgeTmp.second = edges[i]["STOP"].asInt();
                    
            _edges.push_back(edgeTmp);
        }//end for 
        
        const Json::Value& weights = obj["weights"];
        for(int i = 0; i < weights.size(); i++){
            _weights.push_back(weights[i].asInt());
        }//end for 
        
        _weightMap = get(edge_weight,_graph);
        
        // Add edges to the graph
        for(std::size_t j = 0; j < _edges.size() ; ++j) {
            edge_descriptor_t e; bool inserted;
            boost::tie(e, inserted) = add_edge(_edges[j].first,
                                               _edges[j].second, _graph);
            _weightMap[e] = _weights[j];
        }//end for 
        
        
    }//end init function
    
    void run() override{
        vertex_descriptor_t start_vertex  = vertex(1,_graph);
        vertex_descriptor_t target_vertex = vertex(3,_graph);
        
        std::cout << "Num vertices: " << num_vertices(_graph) << std::endl;
        
        Location locations[] = { // lat/long
        {42.73, 73.68}, {44.28, 73.99}, {44.70, 73.46},
        {44.93, 74.89}, {43.97, 75.91}, {43.10, 75.23},
        {43.04, 76.14}, {43.17, 77.61}, {42.89, 78.86},
        {42.44, 76.50}, {42.10, 75.91}, {42.04, 74.11},
        {40.67, 73.94}
      };
        
        // Call Astart search
        std::vector<graph_t::vertex_descriptor> p(num_vertices(_graph));
        std::vector<cost>                       d(num_vertices(_graph));
        
    // call astar named parameter interface
    try{
        astar_search
          (_graph, start_vertex,
           distance_heuristic<graph_t, cost, Location*>
            (locations, target_vertex),
           predecessor_map(&p[0]).distance_map(&d[0]).
           visitor(astar_goal_visitor<vertex_descriptor_t>(target_vertex)));
        std::cout << "Result" << std::endl;
    }
    catch(found_goal fg) { // found a path to the goal
        list<vertex_descriptor_t> shortest_path;
        std::cout << "Catch\n";
        for(vertex_descriptor_t v = target_vertex;; v = p[v]) {
          shortest_path.push_front(v);
          if(p[v] == v)
            break;
        }
        std::cout << "Shortest path size: " << shortest_path.size() << std::endl;
        cout << "Shortest path from " << start_vertex << " to "
             << target_vertex << ": ";
//        list<vertex>::iterator spi = shortest_path.begin();
//        cout << name[start];
//        for(++spi; spi != shortest_path.end(); ++spi)
//          cout << " -> " << name[*spi];
////        cout << endl << "Total travel time: " << d[goal] << endl;
//        return 0;
  }
        
    }//end run function 
    
    
    void write() override{
         const char *name[] = {
            "Troy", "Lake Placid", "Plattsburgh", "Massena",
            "Watertown", "Utica", "Syracuse", "Rochester", "Buffalo",
            "Ithaca", "Binghamton", "Woodstock", "New York", "N"
          };
         
        Location locations[] = { // lat/long
        {42.73, 73.68}, {44.28, 73.99}, {44.70, 73.46},
        {44.93, 74.89}, {43.97, 75.91}, {43.10, 75.23},
        {43.04, 76.14}, {43.17, 77.61}, {42.89, 78.86},
        {42.44, 76.50}, {42.10, 75.91}, {42.04, 74.11},
        {40.67, 73.94}
      };
        ofstream dotfile;
        dotfile.open("test-astar-cities.dot");
        write_graphviz(dotfile, _graph,
                       city_writer<const char **, Location*>
                        (name, locations, 73.46, 78.86, 40.67, 44.93,
                         480, 400),
                       time_writer<weightMap_t>(_weightMap));
    }//end write function 
    
    
    
    
    
    
public:
    std::vector<int>      _nodes;
    std::vector<Location> _locations;
    std::vector<edge_t>   _edges;
    std::vector<int>      _weights;
    weightMap_t           _weightMap;
    graph_t               _graph;
    
};//end class 

};//end namespace 



#endif /* GRAPHBASE_H */

