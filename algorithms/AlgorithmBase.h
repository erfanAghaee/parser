/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AlgorithmBase.h
 * Author: erfan
 *
 * Created on October 15, 2019, 11:29 AM
 */

#ifndef ALGORITHMBASE_H
#define ALGORITHMBASE_H

namespace UCal{
    
class AlgorithmBase{
public:
    virtual void init(){}
    virtual void run(){}
    virtual void write(){}
};// end class

};// end namespace 


#endif /* ALGORITHMBASE_H */

