/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   GraphBaseV2.h
 * Author: erfan
 *
 * Created on October 16, 2019, 9:12 AM
 */

#ifndef GRAPHBASEV2_H
#define GRAPHBASEV2_H


#include <iostream>
#include <fstream>
#include <list>

#include <boost/graph/astar_search.hpp>
#include <boost/graph/graphviz.hpp>

#include <jsoncpp/json/json.h>

#include "algorithms/AlgorithmBase.h"
#include "../basics/Utils.h"



namespace UCal{
  using namespace boost;
  using namespace std;
  
  

  // specify some types
  typedef float cost;
  typedef adjacency_list<listS, vecS, undirectedS, no_property,
    property<edge_weight_t, cost> >                  graph_t;
  typedef property_map<graph_t, edge_weight_t>::type weight_map_t;
  typedef graph_t::vertex_descriptor                 vertex_descriptor_t;
  typedef graph_t::edge_descriptor                   edge_descriptor_t;
  typedef graph_t::vertex_iterator                   vertex_iterator_t;
  typedef std::pair<int, int>                        edge_t;
  
  template<class Name>
  class NodeWriter{
    public:
        NodeWriter(Name name): 
                    _name(name)
                {}//end constructor 
        
        template <class Vertex>
        void operator()(ostream& out, const Vertex& v) const {
            out << "[label=\"" << _name[v] << "\"]";
        }//end operator
    private:
        Name               _name;     
  };//end class graphwriter 
  
  template<class WeightMap>
  class EdgeWriter{
    public:
        EdgeWriter(WeightMap weight_map): 
                    _weight_map(weight_map)
                {}//end constructor 
        
        template <class Edge>
        void operator()(ostream& out, const Edge& e) const {
            out << "[label=\"" << _weight_map[e] << "\"]";
        }//end operator
    private:
        WeightMap      _weight_map;     
  };//end class graphwriter 
  
  
  // euclidean distance heuristic
  template<class Graph, class CostType, class LocMap>
  class DistanceHeuristic
  {
    public:
        typedef typename graph_traits<Graph>::vertex_descriptor Vertex;
        DistanceHeuristic(LocMap loc_map, Vertex goal_vertex) :
                _goal_vertex(goal_vertex)
            ,   _location_map(loc_map)
        {}
        
        CostType operator()(Vertex u)
        {
            CostType dx= _location_map[_goal_vertex].x - _location_map[u].x;
            CostType dy= _location_map[_goal_vertex].y - _location_map[u].y;
            return ::sqrt(dx*dx+dy*dy);
        }//end operator function
    private:
        Graph       _graph;
        CostType    _cost_type;
        LocMap      _location_map;
        Vertex      _goal_vertex;
  };//end class 
  
  
  // visitor that terminates when we find the goal
  template <class Vertex>
  class AStarGoalVisitor : public boost::default_astar_visitor
  {
  public:
    AStarGoalVisitor(Vertex goal) : _goal_vertex(goal) {}
    template <class Graph>
    void examine_vertex(Vertex u, Graph& g) {
      if(u == _goal_vertex)
        throw found_goal();
    }
  private:
    Vertex _goal_vertex;
  };

  
  class GraphBaseV2 {
  public: 
      GraphBaseV2() {}
      ~GraphBaseV2() {}
      
      void loadJson(std::string& json_file) {
        ifstream ifs("json/loadGraph.json");
        Json::Reader  reader;
        Json::Value   obj;
        reader.parse(ifs, obj); // reader can also read strings
        const Json::Value& nodes = obj["nodes"];
        for(int i = 0; i < nodes.size(); i++){
            _nodes.push_back(nodes[i].asInt());
            _names.push_back(nodes[i].asInt());
        }//end for 
        const Json::Value& locations = obj["locations"];
        for(int i = 0; i < locations.size(); i++){
            Location locationTmp;
            locationTmp.x = locations[i]["X"].asFloat();
            locationTmp.y = locations[i]["Y"].asFloat();
            _locations.push_back(locationTmp);
        }//end for 
        
        const Json::Value& edges = obj["edges"];
        for(int i = 0; i < edges.size(); i++){
            edge_t edgeTmp;
            edgeTmp.first  = edges[i]["START"].asInt();
            edgeTmp.second = edges[i]["STOP"].asInt();
            _edges.push_back(edgeTmp);
        }//end for 
        
        const Json::Value& weights = obj["weights"];
        for(int i = 0; i < weights.size(); i++){
            _weights.push_back(weights[i].asInt());
        }//end for 
        
        _weight_map = get(edge_weight,_graph);
        
        // Add edges to the graph
        for(std::size_t j = 0; j < _edges.size() ; ++j) {
            edge_descriptor_t e; bool inserted;
            boost::tie(e, inserted) = add_edge(_edges[j].first,
                                               _edges[j].second, _graph);
            _weight_map[e] = _weights[j];
        }//end for 
      }//end function load josn file
      
      void writeGraph(std::string& json_file){
        ofstream dotfile;
        dotfile.open("test-astar-cities.dot");   
         write_graphviz(
                dotfile,
                _graph,
                NodeWriter<std::vector<int> >
                    (_names),
                EdgeWriter<weight_map_t>(_weight_map));
      }//end function writeGraph
      
      void run(const vertex_descriptor_t& start_vertex,  
        const vertex_descriptor_t& target_vertex, 
        std::string& algorithm_type)
      {
        std::vector<graph_t::vertex_descriptor> p(num_vertices(_graph));
        std::vector<cost>                       d(num_vertices(_graph));
        try{
            astar_search(_graph,
                start_vertex,
                DistanceHeuristic<graph_t, cost, std::vector<Location>>
                (_locations, target_vertex),
                predecessor_map(&p[0]).distance_map(&d[0]).visitor(AStarGoalVisitor<vertex_descriptor_t>(target_vertex))
                );
        }
        catch(found_goal fg)// found a path to the goal
        {
            list<vertex_descriptor_t> shortest_path;
            for(vertex_descriptor_t v = target_vertex;; v = p[v]) {
                shortest_path.push_front(v);
                if(p[v] == v)
                  break;
            }//end for     
            list<vertex_descriptor_t>::iterator spi = shortest_path.begin();
            cout << _names[start_vertex];
            for(++spi; spi != shortest_path.end(); ++spi)
                  cout << " -> " << _names[*spi];
            cout << endl << "Total travel time: " << d[target_vertex] << endl;
            
        }//end try
      }//end run function
      
  private:
    std::vector<int>      _nodes;
    std::vector<Location> _locations;
    std::vector<edge_t>   _edges;
    std::vector<int>      _weights;
    weight_map_t          _weight_map;
    graph_t               _graph;    
    std::vector<int>      _names;
      
  };//end class 
  
  
};//end namespace 


#endif /* GRAPHBASEV2_H */

