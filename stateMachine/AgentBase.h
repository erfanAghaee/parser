/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AgentBase.h
 * Author: erfan
 *
 * Created on October 12, 2019, 2:39 PM
 */

#ifndef AGENTBASE_H
#define AGENTBASE_H

#include <iostream>

#include <boost/statechart/event.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/transition.hpp>
//front-end
#include <boost/msm/front/state_machine_def.hpp>
// functors
#include <boost/msm/front/functor_row.hpp>
#include <boost/msm/front/euml/common.hpp>

//#include "../logger/Logger.h"

namespace UCal{
    namespace msm = boost::msm;
namespace mpl = boost::mpl;
using namespace boost::msm::front;


namespace sc = boost::statechart;
class AgentBase{
public: 
    struct Init;
    
    struct AgentStateMachine : sc::state_machine<AgentStateMachine,Init>{
        AgentStateMachine(){
            std::cout << "Constructor AgentStateMachine!\n";
        }//end constructor 
        ~AgentStateMachine(){
            std::cout << "Deconstructor AgentStateMachine!\n";
        }//end constructor 
        
        typedef sc::transition<none,Init> reactions;
    };//end AgentStateMachine struct 
    
       
    
    struct Init : sc::simple_state<Init, AgentStateMachine>{
        Init(){
            std::cout << "Constructor Init!\n";
        }//end constructor 
        ~Init(){
            std::cout << "Deconstructor Init!\n";
        }//end constructor 
    };//end Init struct 
  
      
    struct Idle : sc::simple_state<Idle, AgentStateMachine>{
        Idle(){
            std::cout << "Constructor Idle!\n";
        }//end constructor 
        ~Idle(){
            std::cout << "Deconstructor Idle!\n";
        }//end constructor 
    };//end Init struct 
    
};//end 

};// end namespace 


#endif /* AGENTBASE_H */

