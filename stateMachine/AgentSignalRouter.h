/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   AgentSignalRouter.h
 * Author: erfan
 *
 * Created on October 13, 2019, 10:59 AM
 */

#ifndef AGENTSIGNALROUTER_H
#define AGENTSIGNALROUTER_H

#include "AgentBase.h"

namespace UCal{

class AgentSignalRouter : public AgentBase{
public:
    AgentSignalRouter();
    AgentSignalRouter(const AgentSignalRouter& orig);
    virtual ~AgentSignalRouter();
    
    
private:
    
    

};//end class AgentSignalRouter

};//end namespace 

#endif /* AGENTSIGNALROUTER_H */

