/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Camera.h
 * Author: erfan
 *
 * Created on October 13, 2019, 12:00 PM
 */

#ifndef CAMERA_H
#define CAMERA_H


#include "../logger/Logger.h"

#include <boost/statechart/event.hpp>
#include <boost/statechart/state_machine.hpp>
#include <boost/statechart/simple_state.hpp>
#include <boost/statechart/custom_reaction.hpp>
#include <boost/thread/thread.hpp>
#include <boost/chrono/chrono.hpp>


namespace UCal{
    
namespace sc  = boost::statechart;
namespace mpl = boost::mpl;

    
class Camera{
public:
    struct EvShutterHalf    : sc::event< EvShutterHalf > {};
    struct EvShutterFull    : sc::event< EvShutterFull > {};
    struct EvShutterRelease : sc::event< EvShutterRelease > {};
    struct EvConfig         : sc::event< EvConfig > {};
    struct EvInFocus        : sc::event< EvInFocus > {};
    
    struct NotShooting;
    struct Shooting;
    
    struct CameraMachine : sc::state_machine<CameraMachine,NotShooting>{
    public:
        CameraMachine() {BOOST_LOG_TRIVIAL(trace) << "Constructor CameraMachine!\n";}
        ~CameraMachine(){BOOST_LOG_TRIVIAL(trace) << "Deconstructor CameraMachine!\n";}
        
        bool IsMemoryAvailable() const {return true;}
        bool IsBattryLow() const {return false;}
    };
    
    struct Idle;   
    
    
    struct NotShooting : sc::simple_state<NotShooting, CameraMachine, Idle>
    {
        NotShooting() {BOOST_LOG_TRIVIAL(trace) << "Constructor NotShooting!\n";}
        ~NotShooting(){BOOST_LOG_TRIVIAL(trace) << "Deconstructor NotShooting!\n";}
        
        typedef sc::custom_reaction<EvShutterHalf> reactions;
        
        sc::result react(const EvShutterHalf& evShutterHalf){
            if ( context< CameraMachine >().IsBattryLow() )
            {
              return forward_event();
            }
            else
            {
              return transit< Shooting >();
            }
        }//end react function   
    };
    
    struct Focusing;
    struct Shooting : sc::simple_state<Shooting, CameraMachine,Focusing>{
        Shooting() {BOOST_LOG_TRIVIAL(trace) << "Constructor Shooting!\n";}
        ~Shooting(){BOOST_LOG_TRIVIAL(trace) << "Deconstructor Shooting!\n";}
        
        void DisplayFocused(const EvInFocus& evInFocus){
            BOOST_LOG_TRIVIAL(trace) << "DispalyFocused function in Shooting struct!\n";
            
        }//end function
    };

    
    struct Storing : sc::simple_state< Storing,Shooting>{
        Storing() {BOOST_LOG_TRIVIAL(trace) << "Constructor Storing!\n";}
        ~Storing(){BOOST_LOG_TRIVIAL(trace) << "Deconstructor Storing!\n";}
    };//end Storing struct
    

   
//    
    struct Focused : sc::simple_state< Focused, Shooting>{
        Focused() {BOOST_LOG_TRIVIAL(trace) << "Constructor Focused!\n";}
        ~Focused(){BOOST_LOG_TRIVIAL(trace) << "Deconstructor Focused!\n";}
        
        typedef sc::custom_reaction<EvShutterFull> reactions;
        
        sc::result react(const EvShutterFull& evShutterFull){
            if(context<CameraMachine>().IsMemoryAvailable()){
                return transit<Storing>();
            }else{
                BOOST_LOG_TRIVIAL(warning) << "Cache Memory full. Please wait ... !\n";
                return transit<Focused>(); 
            }//end if 
            
//            return 
        };//end struct 
    };//end Storing struct
//    
    struct Focusing : sc::simple_state<Focusing,Shooting>{
        Focusing() {BOOST_LOG_TRIVIAL(trace) << "Constructor Focusing!\n";}
        ~Focusing(){BOOST_LOG_TRIVIAL(trace) << "Deconstructor Focusing!\n";}
        
        typedef sc::transition<EvInFocus,Focused,
            Shooting,&Shooting::DisplayFocused> reactions;
//        typedef mpl::list<
//            sc::custom_reaction<EvInFocus>//,
////            sc::deferral<EvShutterFull>
//        > reactions;
        
//        sc::result react(const EvInFocus& evInFocus){
//            
//        }//end react function   
        
//        sc::result react(const EvShutterFull& evShutterFull){
//            if(context<CameraMachine>().IsMemoryAvailable()){
//                return transit<Storing>();
//            }
//            else
//            {
//                BOOST_LOG_TRIVIAL(warning) << "Cache memory is full. Please wait ...\n";
//                return transit<Focused>();
//            }// end if 
//        }//end react function  
    };
    
    
    
    struct Configuring: sc::simple_state<Configuring, NotShooting>{
        Configuring() {BOOST_LOG_TRIVIAL(trace) << "Constructor Configuring!\n";}
        ~Configuring(){BOOST_LOG_TRIVIAL(trace) << "Deconstructor Configuring!\n";}
    };
    
       
    
    struct Idle : sc::simple_state<Idle, NotShooting>{
        Idle() {
            BOOST_LOG_TRIVIAL(trace) << "Constructor Idle!\n";
//            boost::thread t(boost::bind(&Idle::thread2,this));
//            t.join();
            thread2();
        }
        
        ~Idle(){BOOST_LOG_TRIVIAL(trace) << "Deconstructor Idle!\n";}
        
//        void wait(int seconds){
//            boost::this_thread::sleep_for(boost::chrono::seconds{seconds});
//        }//end wait function
        
        void thread2(){
            for(int i = 1; i < 50000; i++){
//                wait(1);
                std::string str = std::to_string(i) + "number\n"; 
                BOOST_LOG_TRIVIAL(trace) << str;
            }//end for 
        }//end thread 
        
//        typedef sc::transition<Focused> reactions;
        typedef sc::custom_reaction<EvConfig> reactions;
        
        sc::result react(const EvConfig& evConfig){
            return transit<Configuring>();
        }// end react function 
    };//end struct 

};// end class 

};//end namespace 


#endif /* CAMERA_H */

