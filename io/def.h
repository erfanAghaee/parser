#ifndef _IO_DEF_H_
#define _IO_DEF_H_

namespace ispd19{
#define CHECK_STATUS(status) \
  if (status) {              \
     defwPrintError(status); \
     return(status);         \
  }
}

#endif

