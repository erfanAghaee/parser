#include "def.h"

#include "io.h"

using namespace ispd19;

bool IO::readDef(const std::string &def)
{
    FILE *fp = 0;
    if ( !(fp = fopen(def.c_str(), "r") ) )
    {
        std::cerr<<"Unable to open DEF file: "<<def<<std::endl;
        return false;
    }

    defrSetAddPathToNet();

    defrSetDesignCbk    ( IO::readDefDesign );
    defrSetUnitsCbk     ( IO::readDefUnits  );
    defrSetPropCbk      ( IO::readDefProperty );
    defrSetDieAreaCbk   ( IO::readDefDieArea );
    defrSetRowCbk       ( IO::readDefRow );
    defrSetTrackCbk     ( IO::readDefTrack );
    defrSetGcellGridCbk ( IO::readDefGcellGrid );
    defrSetComponentCbk ( IO::readDefComponent );
    defrSetNetCbk       ( IO::readDefNet );
    defrSetSNetCbk      ( IO::readDefSNet );
    defrSetViaCbk       ( IO::readDefVia );

    defrInit();
    defrReset();

    int result = defrRead(fp, def.c_str(), (void*)Database::get(), 1);

    defrReleaseNResetMemory();
    defrUnsetCallbacks();

    fclose(fp);

    if ( result )
    {
        std::cerr<<"Error in parsing DEF file (error: "<<result<<")"<<std::endl;
        return false;
    }
    return true;
}

bool IO::writeDef(const std::string &def)
{
    FILE *fp   = 0;
    int status = 0;
    int res    = 0;
    
    if ( !(fp = fopen(def.c_str(), "w") ) )
    {
        std::cerr<<"Unable to open DEF file: "<<def<<std::endl;
        return false;
    }


    status = defwInitCbk(fp);
    CHECK_STATUS(status);
    
    
    // set the callback functions
//    defwSetArrayCbk         (   arrayCB         );
    defwSetBusBitCbk          (   IO::writeBusBit        );
    defwSetDividerCbk         (   IO::writeDivider       );
    defwSetComponentCbk       (   IO::writeComponent     );
//    defwSetDesignCbk        (   designCB        );
//    defwSetDesignEndCbk     (   (defwVoidCbkFnType)designendCB  );
    defwSetDieAreaCbk         (   IO::writeDiearea       );
//    defwSetExtCbk           (   extensionCB     );
//    defwSetFloorPlanCbk     (   floorplanCB     );
//    defwSetGcellGridCbk     (   gcellgridCB     );
//    defwSetGroupCbk         (   groupCB         );
//    defwSetHistoryCbk       (   historyCB       );
    defwSetNetCbk             (   IO::writeNet           );
//    defwSetPinCbk           (   pinCB           );
//    defwSetPinPropCbk       (   pinpropCB       );
//    defwSetPropDefCbk       (   propdefCB       );
//    defwSetRegionCbk        (   regionCB        );
//    defwSetRowCbk           (   rowCB           );
//    defwSetSNetCbk          (   snetCB          );
//    defwSetTechnologyCbk    (   technologyCB    );
//    defwSetTrackCbk         (   trackCB         );
//    defwSetUnitsCbk         (   unitsCB         );
//    defwSetViaCbk           (   viaCB           );
//

    res = defwWrite(fp,  def.c_str(), (void*)Database::get());
    
    fclose(fp);

    if ( res )
    {
        std::cerr<<"Error in parsing solution DEF file"
                " (error: "<<res<<")"<<std::endl;
        return false;
    }
    
    return true;
}//end writeDef function

/***** DEF call backs *****/

//----- Design -----
int IO::readDefDesign(defrCallbackType_e c, const char *name, defiUserData ud)
{
    Database *db = (Database*) ud;
    db->designName(std::string(name));
    return 0;
}

//----- Unit -----
int IO::readDefUnits(defrCallbackType_e c, double d, defiUserData ud)
{
    Database *db = (Database*) ud;
    db->defDbuPerMicron(d);
    return 0;
}

//----- Property -----
int IO::readDefProperty(defrCallbackType_e c, defiProp *prop, defiUserData ud)
{
    /*
    std::string propType(prop->propType());
    char dataType = prop->dataType();
    std::string propName(prop->propName());

    if( propType == "design" )
    {
    }
    if( propType == "componentpin" )
    {
    }
    std::cout<<propType<<std::endl;
    std::cout<<dataType<<std::endl;
    std::cout<<propName<<std::endl;
    if( prop->hasNumber() )
    {
        std::cout<<prop->number()<<std::endl;
    }
    */
    return 0;
}

//----- Die Area -----
int IO::readDefDieArea(defrCallbackType_e c, defiBox *box, defiUserData ud)
{
    Database *db = (Database*) ud;
    Box boxTmp(box->xl(),box->yl(),box->xh(),box->yh());
    db->dieArea(boxTmp);
    return 0;
}

//----- Row -----
int IO::readDefRow(defrCallbackType_e c, defiRow *defRow, defiUserData ud)
{
    Database *db = (Database*) ud;
    Row &row = db->addRow( std::string(defRow->name()) );
    row.x( defRow->x() );
    row.y( defRow->y() );
    row.numSites( defRow->xNum() );
    row.siteWidth( defRow->xStep() );
    row.isFlip( IO::isFlipY(defRow->orient()) );
    return 0;
}

//----- Track -----
int IO::readDefTrack(defrCallbackType_e c, defiTrack *defTrack, defiUserData ud)
{
    Database *db = (Database*) ud;
    char dir = 'X';
    if( !strcmp( defTrack->macro(), "Y" ) )
        dir = 'H';
    else if( !strcmp( defTrack->macro(), "X" ) )
        dir = 'V';
    int start = defTrack->x();
    int num = defTrack->xNum();
    int step = defTrack->xStep();
    for( int i = 0; i < defTrack->numLayers(); ++i )
    {
        unsigned char layerId = Database::rLayer(std::string(defTrack->layer(i)));
        db->getRouteLayer(layerId).setTrack(dir, start, num, step);
    }
    return 0;
}

//----- GCell -----
int IO::readDefGcellGrid(defrCallbackType_e c, defiGcellGrid *gcell, defiUserData ud)
{
    Database *db = (Database*) ud;
    int gridLoc = gcell->x();
    int gridDo = gcell->xNum();
    int gridStep = gcell->xStep();
    std::string dir(gcell->macro());
    if( dir == "X" ) {
        for( int loc = gridLoc, i = 0; i < gridDo; ++i, loc += gridStep ) {
            db->addGridLineX(loc);
        }
    }
    if( dir == "Y" ) {
        for( int loc = gridLoc, i = 0; i < gridDo; ++i, loc += gridStep ) {
            db->addGridLineY(loc);
        }
    }
    return 0;
}

//----- Component -----
int IO::readDefComponent(defrCallbackType_e c, defiComponent *comp, defiUserData ud)
{
    Database *db = (Database*) ud;
    Instance& inst = db->addInstance( std::string(comp->defiComponent::id()) );
    inst.macro(Database::macro(std::string(comp->defiComponent::name())));
    if( comp->defiComponent::isUnplaced() )
    {
        //unplaced
    }
    else if( comp->defiComponent::isPlaced() || comp->defiComponent::isFixed() )
    {
        int x = comp->defiComponent::placementX();
        int y = comp->defiComponent::placementY();
        bool flipX = IO::isFlipX(comp->defiComponent::placementOrient());
        bool flipY = IO::isFlipY(comp->defiComponent::placementOrient());
        std::string orient = comp->defiComponent::placementOrientStr();
        inst.place(x, y, flipX, flipY);
        inst.orient(orient);
    }
    return 0;
}

//----- Net -----
int IO::readDefNet(defrCallbackType_e c, defiNet *defNet, defiUserData ud)
{
    Database *db = (Database*) ud;
    Net &dNet = db->addNet( std::string( defNet->defiNet::name() ) );

    for( int i = 0; i < defNet->defiNet::numConnections(); ++i )
    {
        std::string instName( defNet->defiNet::instance(i) );
        std::string pinName( defNet->defiNet::pin(i) );
        if( instName == "PIN" )
        {
            //this is a pin
            //std::cout<<"pin : "<<pinName<<std::endl;
            dNet.addPin(pinName);
        }
        else
        {
            //this is a cell
            Instance& inst = db->getInstance(instName);
            Macro& macro = db->getMacro(inst.macro());                    
            macro.addPin(pinName);
//            macro.addPin(pinName);
//            dNet.addPin(pinName);
            dNet.addInstance(instName);
        }
    }
    for( int wireIdx = 0; wireIdx < defNet->defiNet::numWires(); ++wireIdx )
    {
        defiWire *wire = defNet->defiNet::wire(wireIdx);
        if( std::string(wire->wireType()) != "ROUTED" )
            continue;
        for( int pathIdx = 0; pathIdx < wire->numPaths(); ++pathIdx )
        {
            defiPath *path = wire->path(pathIdx);
            path->initTraverse();
            
            std::pair<char,unsigned char> layer;
            unsigned viaId = Via::NullIndex;
            Point p1 = Point::Null;
            Point p2 = Point::Null;
            Box rect;
            for( int pathObj = (int) path->next(); pathObj != DEFIPATH_DONE; pathObj = (int) path->next() )
            {
                int lx, ly, hx, hy;
                int x, y, z;
                switch( pathObj )
                {
                    case DEFIPATH_LAYER:
                        layer = Database::layer(std::string(path->getLayer()));
                        break;
                    case DEFIPATH_VIA:
                        viaId = Database::via(std::string(path->getVia()));
                        break;
                    case DEFIPATH_RECT:
                        path->getViaRect(&lx, &ly, &hx, &hy);
                        rect = Box(lx, ly, hx, hy);
                        break;
                    case DEFIPATH_POINT:
                        path->getPoint(&x, &y);
                        if( p1 == Point::Null ) {
                            p1.x(x);
                            p1.y(y);
                        } else {
                            p2.x(x);
                            p2.y(y);
                        }
                        break;
                    case DEFIPATH_FLUSHPOINT:
                        path->getFlushPoint(&x, &y, &z);
                        if( p1 == Point::Null ) {
                            p1.x(x);
                            p1.y(y);
                        } else {
                            p2.x(x);
                            p2.y(y);
                        }
                        break;
                    default:
                        break;
                }
            }
            if( layer.first == 'r' && layer.second != RouteLayer::NullIndex && p1 != Point::Null && p2 != Point::Null )
            {
                dNet.addWire(p1, p2, layer.second);
            }
            if( viaId != Via::NullIndex ) {
                auto& via = Database::get()->getVia(viaId);
                if( p2 != Point::Null ) {
                    dNet.addVia(p2, via.botLayer(), via.topLayer(), viaId);
                } else if( p1 != Point::Null ) {
                    dNet.addVia(p1, via.botLayer(), via.topLayer(), viaId);
                }
            }

        }
    }
    return 0;
}

//----- Special Net -----

int IO::readDefSNet(defrCallbackType_e c, defiNet *defNet, defiUserData ud)
{
    Database *db = (Database*) ud;
    SNet &dSNet = db->addSNet( std::string( defNet->defiNet::name() ) );
    dSNet.use( std::string(defNet->defiNet::use()) );
    for( int wireIdx = 0; wireIdx < defNet->defiNet::numWires(); ++wireIdx )
    {
        defiWire *wire = defNet->defiNet::wire(wireIdx);
        if( std::string(wire->wireType()) != "ROUTED" )
            continue;

        for( int pathIdx = 0; pathIdx < wire->numPaths(); ++pathIdx )
        {
            defiPath *path = wire->path(pathIdx);
            path->initTraverse();

            std::pair<char,unsigned char> layer;
            char shape = 'x';
            //unsigned viaId = Via::NullIndex;
            Point p1 = Point::Null;
            Point p2 = Point::Null;
            Box rect;
            int width = 0;
            for( int pathObj = (int) path->next(); pathObj != DEFIPATH_DONE; pathObj = (int) path->next() )
            {
                int x, y, z;
                switch( pathObj )
                {
                    case DEFIPATH_LAYER:
                        layer = Database::layer(std::string(path->getLayer()));
                        break;
                    case DEFIPATH_VIA:
                        break;
                    case DEFIPATH_RECT:
                        //std::cout<<"box"<<std::endl;
                        break;
                    case DEFIPATH_WIDTH:
                        width = path->getWidth();
                        break;
                    case DEFIPATH_POINT:
                        path->getPoint(&x, &y);
                        if( p1 == Point::Null ) {
                            p1.x(x);
                            p1.y(y);
                        } else {
                            p2.x(x);
                            p2.y(y);
                        }
                        break;
                    case DEFIPATH_FLUSHPOINT:
                        path->getFlushPoint(&x, &y, &z);
                        if( p1 == Point::Null ) {
                            p1.x(x);
                            p1.y(y);
                        } else {
                            p2.x(x);
                            p2.y(y);
                        }
                        break;
                    case DEFIPATH_SHAPE:
                        if( strcmp( path->getShape(), "STRIPE" ) == 0 ) {
                            shape = 's';
                        } else if( strcmp( path->getShape(), "RING" ) == 0 ) {
                            shape = 'r';
                        }
                        break;
                    default:
                        break;
                }
            }
            /*
               std::cout<<"------"<<std::endl;
               std::cout<<"layer type : "<<layer.first<<std::endl;
               std::cout<<"layer      : "<<(int)layer.second<<std::endl;
               std::cout<<"shape      : "<<shape<<std::endl;
               std::cout<<"p1         : "<<p1.toString()<<std::endl;
               std::cout<<"p2         : "<<p2.toString()<<std::endl;
               std::cout<<"via        : "<<viaId<<std::endl;
               std::cout<<"width      : "<<width<<std::endl;
            */
            if( layer.first == 'r' && layer.second != RouteLayer::NullIndex && p1 != Point::Null && p2 != Point::Null ) {
                if( shape == 's' ) {
                    dSNet.addStripe( p1, p2, layer.second, width );
                } else if( shape == 'r' ) {
                    dSNet.addRing( p1, p2, layer.second, width );
                }
            }
        }
    }
    //dSNet.report();
    return 0;
}

//----- Via -----

int IO::readDefVia(defrCallbackType_e c, defiVia *defVia, defiUserData ud )
{
    Database *db = (Database*) ud;
    //double scale = (double) db->defDbuPerMicron();

    SVia &svia = db->addSVia( std::string( defVia->defiVia::name() ) );
    int numRects = defVia->defiVia::numLayers();
    if( numRects > 0 ) {
        unsigned char botLayer = RouteLayer::NullIndex;
        unsigned char cutLayer = CutLayer::NullIndex;
        unsigned char topLayer = RouteLayer::NullIndex;
        for( int i = 0; i < numRects; ++i ) {
            char *name;
            int lx, ly, hx, hy;
            defVia->defiVia::layer(i, &name, &lx, &ly, &hx, &hy);
            auto layer = Database::layer( std::string(name) );
            if( layer.first == 'c' ) {
                if( cutLayer == CutLayer::NullIndex ) {
                    cutLayer = layer.second;
                }
            } else if( layer.first == 'r' ) {
                if( botLayer == RouteLayer::NullIndex ) {
                    botLayer = layer.second;
                } else if( topLayer == RouteLayer::NullIndex ) {
                    topLayer = layer.second;
                }
            }
        }
        if( botLayer > topLayer ) {
            std::swap( botLayer, topLayer );
        }
        svia.botLayer( botLayer );
        svia.cutLayer( cutLayer );
        svia.topLayer( topLayer );
        for( int i = 0; i < defVia->defiVia::numLayers(); ++i )
        {
            char *name;
            int lx, ly, hx, hy;
            defVia->defiVia::layer(i, &name, &lx, &ly, &hx, &hy);
            auto layer = Database::layer( std::string(name) );
            if( layer.first == 'c' && layer.second == cutLayer ) {
                svia.addCutShape( lx, ly, hx, hy );
            }
            if( layer.first == 'r' && layer.second == botLayer ) {
                svia.addBotShape( lx, ly, hx, hy );
            }
            if( layer.first == 'r' && layer.second == topLayer ) {
                svia.addTopShape( lx, ly, hx, hy );
            }
        }
    }
    if( defVia->defiVia::hasViaRule() )
    {
        char *ruleName;
        char *layerBot;
        char *layerCut;
        char *layerTop;
        int cutSizeX;
        int cutSizeY;
        int cutSpacingX;
        int cutSpacingY;
        int encLX;
        int encLY;
        int encHX;
        int encHY;
        int numCutRows;
        int numCutCols;
        int originX;
        int originY;
        int offsetLX;
        int offsetLY;
        int offsetHX;
        int offsetHY;
        defVia->defiVia::viaRule(
            &ruleName,                        // VIARULE
            &cutSizeX, &cutSizeY,             // CUTSIZE
            &layerBot, &layerCut, &layerTop,  // LAYERS
            &cutSpacingX, &cutSpacingY,       // CUTSPACING
            &encLX, &encLY, &encHX, &encHY ); // ENCLOSURE
        svia.viaRule( std::string(ruleName) );
        svia.cutSize( cutSizeX, cutSizeY );
        svia.botLayer( Database::layer( std::string( layerBot ) ).second );
        svia.cutLayer( Database::layer( std::string( layerCut ) ).second );
        svia.topLayer( Database::layer( std::string( layerTop ) ).second );
        svia.cutSpacing( cutSpacingX, cutSpacingY );
        svia.enclosure( encLX, encLY, encHX, encHY );

        if( defVia->defiVia::hasRowCol() ) {
            defVia->defiVia::rowCol( &numCutRows, &numCutCols );
            svia.numCutRows( numCutRows );
            svia.numCutCols( numCutCols );
        }
        if( defVia->defiVia::hasOrigin() ) {
            defVia->defiVia::origin( &originX, &originY );
            svia.origin( originX, originY );
        }
        if( defVia->defiVia::hasOffset() ) {
            defVia->defiVia::offset( &offsetLX, &offsetLY, &offsetHX, &offsetHY );
            svia.offset( offsetLX, offsetLY, offsetHX, offsetHY );
        }
    }

    return 0;
}


//----------------Write DEF ------------------------

//--- write busbit
int IO::writeBusBit( defwCallbackType_e c, defiUserData ud)
{
    int status;
    status = defwBusBitChars("[]");
    CHECK_STATUS(status);
    return 0;
}//end function writeBusBit

//--- Divider 
int IO::writeDivider(defwCallbackType_e c, defiUserData ud) {
  int status;
  status = defwDividerChar("/");
  CHECK_STATUS(status);
  return 0;
}//end divider function

//--- Component
// COMPONENTS
int IO::writeComponent(defwCallbackType_e c, defiUserData ud) {
  int status;
  Database *db = (Database*) ud;
  //start def component
  
  status = defwStartComponents(db->instances().size());
  CHECK_STATUS(status);
  
  for(Instance instance :  db->instances())
  {
        status = defwComponentStr(instance.name().c_str(), 
                db->getMacro(instance.macro()).name().c_str(), 0, NULL, NULL, NULL, NULL, NULL,
                0, NULL, NULL, NULL, NULL, "PLACED", instance.x()
                , instance.y(),
                  instance.orient().c_str(), 0, NULL, 0, 0, 0, 0);
                CHECK_STATUS(status);
  }//end for 
  
  
  status = defwEndComponents();

  return 0;
}//end component function

// DIEAREA
int IO::writeDiearea(defwCallbackType_e c, defiUserData ud) {
  int status;
  Database* db = (Database*) ud;
  
  status = defwDieArea(db->dieArea().lx(), db->dieArea().ly(), 
          db->dieArea().hx(), db->dieArea().hy());
  CHECK_STATUS(status);
  status = defwNewLine();
  CHECK_STATUS(status);
  return 0;
}

//-- Net
int IO::writeNet(defwCallbackType_e c , defiUserData ud)
{
  int status;
//  Database* db = (Database*) ud;
//  
//  status = defwStartNets(db->nets().size());
//  CHECK_STATUS(status);
//  
//  for(Net net : db->nets())
//  {
//    status = defwNet(net.name().c_str());
//    CHECK_STATUS(status);
//    
//    std::vector<Pin> pins = net.pins();
//    for(Pin pin : pins){
//        status = defwNetConnection(pin.name().c_str(),pin.,0);
//        CHECK_STATUS(status);
//    }
//  }//end for 
//  
//  status = defwEndNets();
//  CHECK_STATUS(status);
  return 0;

}//end function writeNet
 


