/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   gloablActive.h
 * Author: erfan
 *
 * Created on October 14, 2019, 5:27 PM
 */

#ifndef GLOABLACTIVE_H
#define GLOABLACTIVE_H

#include <iostream>

#include <boost/msm/back/state_machine.hpp>
//front-end
#include <boost/msm/front/state_machine_def.hpp>
// functors
#include <boost/msm/front/functor_row.hpp>
#include <boost/msm/front/euml/common.hpp>

namespace UCal{
namespace msm = boost::msm;
namespace mpl = boost::mpl;
using namespace boost::msm::front;
};//end namespace 

#endif /* GLOABLACTIVE_H */

