/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   StateMachineBase.h
 * Author: erfan
 *
 * Created on October 14, 2019, 4:53 PM
 */

#ifndef STATEMACHINEBASE_H
#define STATEMACHINEBASE_H

#include <iostream>
// back-end


#include "../States/State1.h"
#include "../States/State2.h"

#include "../States/Init.h"
#include "../States/Learn.h"
#include "../States/Write.h"
#include "../States/Interact.h"
#include "../Events/Events.h"



namespace UCal{


class StateMachineBase : public msm::front::state_machine_def<StateMachineBase> 
{
public:
    typedef Init initial_state;
    
    // Transition table for player
    struct transition_table : mpl::vector<
            //    Start     Event         Next      Action               Guard
            //  +---------+-------------+---------+---------------------+----------------------+
            Row < Init      , none           , Interact                             >,
            Row < Interact  , none           , Write                                >,
            Row < Interact  , EventLearning  , Learn                                >
//            //  +---------+-------------+---------+---------------------+----------------------+
//            Row < State3  , none        , State4  , State3ToState4      , always_true          >,
//            Row < State4  , event1      , State1                                               >
//            //  +---------+-------------+---------+---------------------+----------------------+
        > {};

};//end class

};//end namepsace 



#endif /* STATEMACHINEBASE_H */

