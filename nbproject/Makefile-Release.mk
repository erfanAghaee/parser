#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=GNU-Linux
CND_DLIB_EXT=so
CND_CONF=Release
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/db/db.o \
	${OBJECTDIR}/db/net.o \
	${OBJECTDIR}/io/def.o \
	${OBJECTDIR}/io/guide.o \
	${OBJECTDIR}/io/io.o \
	${OBJECTDIR}/io/lef.o \
	${OBJECTDIR}/io/rpt.o \
	${OBJECTDIR}/io/weight.o \
	${OBJECTDIR}/main.o \
	${OBJECTDIR}/stateMachine/AgentSignalRouter.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=-DBOOST_LOG_DYN_LINK -lpthread
CXXFLAGS=-DBOOST_LOG_DYN_LINK -lpthread

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=-Llib/boost -lboost_log -lboost_log_setup -lboost_system -lboost_filesystem -lboost_thread lib/def/libdef.a lib/lef/liblef.a

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/parser

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/parser: lib/def/libdef.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/parser: lib/lef/liblef.a

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/parser: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/parser ${OBJECTFILES} ${LDLIBSOPTIONS} -DBOOST_LOG_DYN_LINK -lpthread

${OBJECTDIR}/db/db.o: db/db.cpp
	${MKDIR} -p ${OBJECTDIR}/db
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/db/db.o db/db.cpp

${OBJECTDIR}/db/net.o: db/net.cpp
	${MKDIR} -p ${OBJECTDIR}/db
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/db/net.o db/net.cpp

${OBJECTDIR}/io/def.o: io/def.cpp
	${MKDIR} -p ${OBJECTDIR}/io
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/io/def.o io/def.cpp

${OBJECTDIR}/io/guide.o: io/guide.cpp
	${MKDIR} -p ${OBJECTDIR}/io
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/io/guide.o io/guide.cpp

${OBJECTDIR}/io/io.o: io/io.cpp
	${MKDIR} -p ${OBJECTDIR}/io
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/io/io.o io/io.cpp

${OBJECTDIR}/io/lef.o: io/lef.cpp
	${MKDIR} -p ${OBJECTDIR}/io
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/io/lef.o io/lef.cpp

${OBJECTDIR}/io/rpt.o: io/rpt.cpp
	${MKDIR} -p ${OBJECTDIR}/io
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/io/rpt.o io/rpt.cpp

${OBJECTDIR}/io/weight.o: io/weight.cpp
	${MKDIR} -p ${OBJECTDIR}/io
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/io/weight.o io/weight.cpp

${OBJECTDIR}/main.o: main.cpp
	${MKDIR} -p ${OBJECTDIR}
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/main.o main.cpp

${OBJECTDIR}/stateMachine/AgentSignalRouter.o: stateMachine/AgentSignalRouter.cpp
	${MKDIR} -p ${OBJECTDIR}/stateMachine
	${RM} "$@.d"
	$(COMPILE.cc) -O2 -Iinclude/def -Iinclude/lef -Iinclude/boost -Ilogger -I. -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/stateMachine/AgentSignalRouter.o stateMachine/AgentSignalRouter.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
