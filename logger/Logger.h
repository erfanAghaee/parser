/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Logger.h
 * Author: erfan
 *
 * Created on October 10, 2019, 3:40 PM
 */

#ifndef LOGGER_H
#define LOGGER_H

#include "global.h"

#include "boost/log/core.hpp"
#include "boost/log/trivial.hpp"
#include "boost/log/utility/setup/file.hpp"
#include "boost/log/expressions.hpp"

namespace UCal{
       
namespace logging = boost::log;    

class Logger {
public:
    static Logger* get(){
        if(!_instance){
            _instance = new Logger();
        }//end if 
        return _instance;
    }//end static get 
    
    inline void initLogging(){
        logging::add_file_log("logs/sample.log");
        
//        logging::core::get()->set_filter(
//            logging::trivial::severity >= logging::trivial::info
//        );
    }//end initLogging

private:
    static Logger *_instance;
};

Logger *Logger::_instance = 0;  

};//end namespace 

#endif /* LOGGER_H */

