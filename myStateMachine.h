///*
// * To change this license header, choose License Headers in Project Properties.
// * To change this template file, choose Tools | Templates
// * and open the template in the editor.
// */
//
///* 
// * File:   myStateMachine.h
// * Author: erfan
// *
// * Created on October 14, 2019, 4:03 PM
// */
//
//#ifndef MYSTATEMACHINE_H
//#define MYSTATEMACHINE_H
//
//#include <iostream>
//// back-end
//#include <boost/msm/back/state_machine.hpp>
////front-end
//#include <boost/msm/front/state_machine_def.hpp>
//// functors
//#include <boost/msm/front/functor_row.hpp>
//#include <boost/msm/front/euml/common.hpp>
//
//
//
//namespace UCal{
//namespace msm = boost::msm;
//namespace mpl = boost::mpl;
//using namespace boost::msm::front;
//// The list of FSM states
//struct State1 : public msm::front::state<> 
//{
//    // every (optional) entry/exit methods get the event passed.
//    template <class Event,class FSM>
//        void on_entry(Event const&,FSM& ) {std::cout << "entering: State1" << std::endl;}
//    template <class Event,class FSM>
//        void on_exit(Event const&,FSM& ) {std::cout << "leaving: State1" << std::endl;}
//};
//
//struct State2 : public msm::front::state<> 
//{ 
//public:
//    template <class Event,class FSM>
//    void on_entry(Event const& ,FSM&) {std::cout << "entering: State2" << std::endl;}
//    template <class Event,class FSM>
//    void on_exit(Event const&,FSM& ) {std::cout << "leaving: State2" << std::endl;}
//};
//
//struct State3 : public msm::front::state<> 
//{ 
//public:
//    // when stopped, the CD is loaded
//    template <class Event,class FSM>
//    void on_entry(Event const& ,FSM&) {std::cout << "entering: State3" << std::endl;}
//    template <class Event,class FSM>
//    void on_exit(Event const&,FSM& ) {std::cout << "leaving: State3" << std::endl;}
//};
//
//struct State4 : public msm::front::state<>
//{
//public:
//    template <class Event,class FSM>
//    void on_entry(Event const&,FSM& ) {std::cout << "entering: State4" << std::endl;}
//    template <class Event,class FSM>
//    void on_exit(Event const&,FSM& ) {std::cout << "leaving: State4" << std::endl;}
//};
//
//
//// transition actions
//struct State2ToState3 
//{
//public:
//    template <class EVT,class FSM,class SourceState,class TargetState>
//    void operator()(EVT const& ,FSM& ,SourceState& ,TargetState& )
//    {
//        std::cout << "my_machine::State2ToState3" << std::endl;
//    }
//};
//struct State3ToState4 
//{
//public:
//    template <class EVT,class FSM,class SourceState,class TargetState>
//    void operator()(EVT const& ,FSM& ,SourceState& ,TargetState& )
//    {
//        std::cout << "my_machine::State3ToState4" << std::endl;
//    }
//};
//// guard conditions
//struct always_true 
//{
//    template <class EVT,class FSM,class SourceState,class TargetState>
//    bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
//    {
//        std::cout << "always_true" << std::endl;
//        return true;
//    }
//};
//struct always_false 
//{
//public:
//    template <class EVT,class FSM,class SourceState,class TargetState>
//    bool operator()(EVT const& evt,FSM& fsm,SourceState& src,TargetState& tgt)
//    {
//        std::cout << "always_false" << std::endl;
//        return true;
//    }
//};
//
//struct event1{
//
//};
//
//struct MyMachine : public msm::front::state_machine_def<MyMachine>    
//{
//public:
//    typedef State1 initial_state_;
//    
//    // Transition table for player
//    struct transition_table : mpl::vector<
//        //    Start     Event         Next      Action               Guard
//        //  +---------+-------------+---------+---------------------+----------------------+
//        Row < State1  , none        , State2                                               >,
//        Row < State2  , none        , State3  , State2ToState3                             >,
//        Row < State3  , none        , State4  , none                , always_false         >,
//        //  +---------+-------------+---------+---------------------+----------------------+
//        Row < State3  , none        , State4  , State3ToState4      , always_true          >,
//        Row < State4  , event1      , State1                                               >
//        //  +---------+-------------+---------+---------------------+----------------------+
//    > {};
//    
//};// end class 
//
//};//end namespace 
//
//#endif /* MYSTATEMACHINE_H */
//
