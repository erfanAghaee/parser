/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   State1.h
 * Author: erfan
 *
 * Created on October 14, 2019, 5:03 PM
 */

#ifndef STATE1_H
#define STATE1_H



#include <iostream>
// back-end
#include <boost/msm/back/state_machine.hpp>
//front-end
#include <boost/msm/front/state_machine_def.hpp>
// functors
#include <boost/msm/front/functor_row.hpp>
#include <boost/msm/front/euml/common.hpp>

namespace UCal{
namespace msm = boost::msm;
namespace mpl = boost::mpl;
using namespace boost::msm::front;

class State1 : public msm::front::state<>  {
public:
    State1();
    State1(const State1& orig);
    virtual ~State1();
    
    template <class Event,class FSM>
        void on_entry(Event const&,FSM& ) {std::cout << "entering: State1" << std::endl;}
    template <class Event,class FSM>
        void on_exit(Event const&,FSM& ) {std::cout << "leaving: State1" << std::endl;}
    
private:

};//end class 

};//end namespace 

#endif /* STATE1_H */

