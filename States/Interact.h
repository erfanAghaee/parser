/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Interact.h
 * Author: erfan
 *
 * Created on October 14, 2019, 5:20 PM
 */

#ifndef INTERACT_H
#define INTERACT_H

#include "../stateMachineV2/gloablActive.h"

namespace UCal{

class Interact : public msm::front::state<> {
public:
    Interact();
    Interact(const Interact& orig);
    virtual ~Interact();
    
    template <class Event,class FSM>
        void on_entry(Event const&,FSM& ) {
            std::cout << "entering: Interact State" << std::endl;
            
        }
    template <class Event,class FSM>
        void on_exit(Event const&,FSM& ) {std::cout << "leaving: Interact State" << std::endl;}
private:

};//end class 

};//end namespace 

#endif /* INTERACT_H */

