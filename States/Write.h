/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Write.h
 * Author: erfan
 *
 * Created on October 14, 2019, 5:20 PM
 */

#ifndef WRITE_H
#define WRITE_H
#include "../stateMachineV2/gloablActive.h"

namespace UCal{
class Write : public msm::front::state<> {
public:
    Write();
    Write(const Write& orig);
    virtual ~Write();
    
    template <class Event,class FSM>
        void on_entry(Event const&,FSM& ) {std::cout << "entering: Write State" << std::endl;}
    template <class Event,class FSM>
        void on_exit(Event const&,FSM& ) {std::cout << "leaving: Write State" << std::endl;}
private:

};//end class

};//end namespace 

#endif /* WRITE_H */

