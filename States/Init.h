/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Init.h
 * Author: erfan
 *
 * Created on October 14, 2019, 5:19 PM
 */

#ifndef INIT_H
#define INIT_H

#include "../stateMachineV2/gloablActive.h"

namespace UCal{

class Init : public msm::front::state<>  {
public:
    Init();
    Init(const Init& orig);
    virtual ~Init();
    
    template <class Event,class FSM>
        void on_entry(Event const&,FSM& ) {std::cout << "entering: Init State" << std::endl;}
    template <class Event,class FSM>
        void on_exit(Event const&,FSM& ) {std::cout << "leaving: Init State" << std::endl;}
private:

};//end class

};//end namespace 

#endif /* INIT_H */

