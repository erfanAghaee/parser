/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   State2.h
 * Author: erfan
 *
 * Created on October 14, 2019, 5:09 PM
 */

#ifndef STATE2_H
#define STATE2_H

#include <iostream>
// back-end
#include <boost/msm/back/state_machine.hpp>
//front-end
#include <boost/msm/front/state_machine_def.hpp>
// functors
#include <boost/msm/front/functor_row.hpp>
#include <boost/msm/front/euml/common.hpp>

namespace UCal{
namespace msm = boost::msm;
namespace mpl = boost::mpl;
using namespace boost::msm::front;

class State2 : public msm::front::state<> {
public:
    State2();
    State2(const State2& orig);
    virtual ~State2();
    
    template <class Event,class FSM>
    void on_entry(Event const& ,FSM&) {std::cout << "entering: State2" << std::endl;}
    
    template <class Event,class FSM>
    void on_exit(Event const&,FSM& ) {std::cout << "leaving: State2" << std::endl;}
private:

};//end class

};//end namespace 

#endif /* STATE2_H */

