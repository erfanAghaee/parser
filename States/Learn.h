/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/* 
 * File:   Learn.h
 * Author: erfan
 *
 * Created on October 14, 2019, 5:20 PM
 */

#ifndef LEARN_H
#define LEARN_H
#include "../stateMachineV2/gloablActive.h"

namespace UCal{

class Learn : public msm::front::state<> {
public:
    Learn();
    Learn(const Learn& orig);
    virtual ~Learn();
    
    template <class Event,class FSM>
        void on_entry(Event const&,FSM& ) {std::cout << "entering: Learn State" << std::endl;}
    template <class Event,class FSM>
        void on_exit(Event const&,FSM& ) {std::cout << "leaving: Learn State" << std::endl;}
private:

};//end class

};//end namespace 

#endif /* LEARN_H */

