#include <thread>
#include <chrono>

#include "global.h"
#include "db/db.h"
#include "io/io.h"
#include "setting.h"


#include <boost/locale.hpp>
#include <iostream>

#include <ctime>

using namespace ispd19;

class Test{
public:
    std::vector<Pin*> _pins;
    
    void addPin(Pin& pin){_pins.push_back(&pin);}
    
};//end class 

int main(int argc, char **argv)
{
    if( !Setting::readArgs(argc, argv) )
        return 1;

    std::vector<std::string> errors;
    std::vector<std::string> warnings;
    
    //check required files
    if( Setting::techLef().empty() )
        errors.push_back("no LEF file is specified.");
    if( Setting::designDef().empty() )
        errors.push_back("no DEF file is specified.");
    if( Setting::guideRg().empty() )
        warnings.push_back("no route guide is specified.");

    //loading
    if( errors.empty() && !IO::readLef(Setting::techLef()) )
        errors.push_back("load LEF file fail.");
    if( errors.empty() && !IO::readDef(Setting::designDef()) )
        errors.push_back("load DEF file fail.");
    if( errors.empty() && !Setting::guideRg().empty() && !IO::readRg(Setting::guideRg()) )
        warnings.push_back("load guide file fail.");
    if( errors.empty() && !Setting::solutionDef().empty() && !IO::writeDef(Setting::solutionDef()) )
        warnings.push_back("write def file fail.");
//
//    Database::get()->reportNets();
//    Database::get()->reportMacros();
//    Database::get()->reportLayers();
    
    Net& net = Database::get()->getNet(1);
    std::cout << "Number of pins: " << net.numPins() << std::endl;
    
//    std::vector<Pin> pins = net.pins();
    std::vector<Instance> instances = net.instances();
    for(Instance instance : instances){
        std::cout << "Instance Name: " << instance.name() << std::endl;
        std::vector<Pin*> pins = instance.pins();
        for(Pin* pin : pins){
            std::cout << "Pin: " << pin->name() << std::endl;
        }
        
    }//end for 
    
//    Macro& macro = Database::get()->getMacro(1);
//    macro.report();
    
    
    for( const std::string &error : errors ) {
        std::cout<<"[ERROR] "<<error<<std::endl;
    }
    for( const std::string &warning : warnings ) {
        std::cout<<"[WARNING] "<<warning<<std::endl;
    }
    
    

    
    return 0;
}
//
//
//
//#include <boost/geometry.hpp>
//#include <boost/geometry/geometries/geometries.hpp>
//#include <iostream>
//namespace bg = boost::geometry;
//int main() {  
//using point          = bg::model::point<double,2,bg::cs::cartesian>;
//using linestring     = bg::model::linestring<point>;
//using polygon        = bg::model::polygon<point>;
//using multi_polygon  = bg::model::multi_polygon<polygon>;
//
//linestring      ls;
//polygon         poly;
//multi_polygon   mpoly;
//
//point p1,p2;
//p1.set<0>(1);
//p1.set<1>(2);
//p2.set<0>(3);
//p2.set<1>(4);
//std::cout << p1.get<0>() << " " << p1.get<1>();
//bg::append(ls,p1);
//bg::append(ls,p2);
//double l = bg::length(ls);
//std::cout << "length: " << l << std::endl;
//bg::append(poly,ls);
////bg::read_wkt("LINESTRING(0 3, 3 0, 4 0)", ls);
////bg::read_wkt("POLYGON((0 0, 0 1, 1 1, 1 0, 0 0))", poly);
////bg::read_wkt("MULTIPOLYGON(((2 2,2 3,3 2,2 2)),((3 2,3 3,4 2,3 2)))",mpoly);
////std::cout << bg::distance(ls, poly) << '\n'<< bg::distance(ls, mpoly);
//return 0;
//}

//#include <boost/geometry.hpp>
//#include <boost/geometry/geometries/register/point.hpp>
//#include <boost/geometry/geometries/register/linestring.hpp>
//#include <boost/geometry/geometries/register/multi_polygon.hpp>
//
//
//#include <iostream>
//#include <vector>
//
//namespace bg = boost::geometry;
//
//struct my_point{
//    double x,y;
//};//end my_point
//
//BOOST_GEOMETRY_REGISTER_POINT_2D(my_point, double, bg::cs::cartesian, x, y)
//BOOST_GEOMETRY_REGISTER_LINESTRING(std::vector<my_point>)
//
//
//int main(){
//    my_point pt{0, 0};  
//    std::vector<my_point> ls{{0, 1}, {1, 0}, {1, 1}, {0.5, 1}};
//    std::cout << bg::distance(pt, ls);
//}//end main

//#include <boost/geometry.hpp>
//#include <boost/geometry/geometries/geometries.hpp>
//#include <boost/geometry/index/rtree.hpp>
//#include <iostream>
//#include <vector>
//namespace bg = boost::geometry;
//
//using point     = bg::model::point<double, 2, bg::cs::cartesian>;
//using polygon   = bg::model::polygon<point>;
//using box       = bg::model::box<point>;
//
//using value     = std::pair<box,size_t>;
//using rtree     = bg::index::rtree<value,bg::index::rstar<16>>;
//
//int main(){
//    std::vector<polygon> polys(4);
//    bg::read_wkt("POLYGON((0 0, 0 1, 1 0, 0 0))", polys[0]); 
//    bg::read_wkt("POLYGON((1 1, 1 2, 2 1, 1 1))", polys[1]); 
//    bg::read_wkt("POLYGON((2 2, 2 3, 3 2, 2 2))", polys[2]); 
//    bg::read_wkt("POLYGON((3 3, 3 4, 4 3, 3 3))", polys[3]);
//    
//    rtree rt;
//    for(std::size_t i = 0; i < polys.size(); ++i)
//    {
//        box b = bg::return_envelope<box>(polys[i]);
//        rt.insert(std::make_pair(b,i));
//    }//end for 
//    
//    polygon qpoly; 
//    
//    bg::read_wkt("POLYGON((0.25 0.25,0.5 1.5,0.9 0.9,1.5 0.5,0.25 0.25))",qpoly);
//    
//    box qbox = bg::return_buffer<box>(bg::return_envelope<box>(qpoly),0.0001);
//    
//    std::vector<value> result;
//    rt.query(bg::index::intersects(qbox),std::back_inserter(result));
//    
//    for(value const& v : result)
//    {
//        std::cout << bg::wkt(polys[v.second])
//                  << (bg::intersects(polys[v.second],qpoly) ? 
//                      "intersected" : "not intersected" ) << std::endl;
//    }//end for 
//    
//}//end main function